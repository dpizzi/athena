/*
  Copyright (C) 2002-2021 CERN for the benefit of the ATLAS collaboration
*/

#include "src/ActsSeedingTool.h" 
#include "src/ActsSeedingAlgorithm.h"
#include "src/ActsSeedingFromAthenaAlgorithm.h"
#include "src/ActsSiSpacePointsSeedMaker.h"

DECLARE_COMPONENT( ActsTrk::ActsSeedingTool )
DECLARE_COMPONENT( ActsTrk::ActsSeedingAlgorithm )
DECLARE_COMPONENT( ActsTrk::ActsSeedingFromAthenaAlgorithm )
DECLARE_COMPONENT( ActsTrk::ActsSiSpacePointsSeedMaker )
